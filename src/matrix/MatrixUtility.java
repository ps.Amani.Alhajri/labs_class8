/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix;

/**
 * @author Amani Al-hajri
 */
public class MatrixUtility {

    // TODO remove those variables
   

    // 1. Sum of two arrays: It shall accept two instances of dimensional arrays that has the same size 
    //  (columns and rows numbers) then returns the sum of both arrays in a new array, example:
    public int[][] sumMatrices(int[][] first, int[][] second) throws IllegalArgumentException {

        // Check if the rows are not the same:
        if (first.length != second.length) {
            throw new IllegalArgumentException("Ops, The rows of two arrays are not the same!!"); // Terminate and throw an error message
        }

        // Check if the rows are not the same:
        if (first[0].length != second[0].length) {
            throw new IllegalArgumentException("Ops, The columns of two arrays are not the same!!"); // Terminate and throw an error message
        }

        // TODO result matrix should have dimensions according to inputs, apply to all results
        int[][] result = new int[first.length][first[0].length];

        // Get the sum of 2 2D array:
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < first[i].length; j++) {
                result[i][j] = first[i][j] + second[i][j];
            }
        }

        return result; // Return the result

    }

    // 2.	Scalar Multiplication: multiply a matrix with a number, the number will be multiplied with each element in the matrix, example below:
    public int[][] scale(int[][] matrix, int scaler) {

        // Creat array var
        int[][] ResultM = new int[matrix.length][matrix[0].length];

        // Multiply a matrix with a number:
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                ResultM[i][j] = scaler * matrix[i][j];
            }
        }

        return ResultM; // Return the result
    }

    // 3.	Transportation: produce a new matrix from another matrix by changing each tow in the matrix to a column.
    public int[][] transpose(int[][] matrix) {

        // Creat array var
        int[][] result = new int[matrix.length][matrix[0].length];

        // Produce a new matrix from another matrix by changing each tow in the matrix to a column
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                result[j][i] = matrix[i][j];
            }
        }
        return result; // Return the result
    }

    // 4.	Matrix Multiplication: Multiplication of two matrices is defined if and only if the number of columns of the 
    // left matrix is the same as the number of rows of the right matrix.
    public int[][] multiply(int[][] first, int[][] second) throws IllegalArgumentException {

        // Check if the size are the same:
        if (first[0].length != second.length) {
            throw new IllegalArgumentException("Ops, The number of columns of the left matrix is not the same as the number of rows of the right matrix!!");
        }

        // Creat array var
        int[][] result = new int[first.length][second[0].length];

        // Get the sum of 2 2D array:
        for (int row = 0; row < first.length; row++) {
            for (int col = 0; col < second[0].length; col++) {
                int sum = 0;
                for (int t = 0; t < first[0].length; t++) {
                    sum += first[row][t] * second[t][col];
                }
                result[row][col] = sum;
            } // End of j loop 
        } // End of row loop

        return result; // Return the result
    }

    // 5.	Submatrix: A submatrix of a matrix is obtained by deleting any collection of rows and/or columns. 
    // For example, from the following 3-by-4 matrix, we can construct a 2-by-3 submatrix by removing rowToRemove 3 and column 2:
    public int[][] subMatrix(int[][] matrix, int rowToRemove, int colToRemove) throws IllegalArgumentException {

        // Check if the size are the same:
        // TODO you should verify another case, if rowToRemove or colToRemove are in minus
        if (rowToRemove > matrix.length || colToRemove > matrix[0].length) 
            throw new IllegalArgumentException("Ops, The n-by-m is greater than the matrix!!");
        
        if (rowToRemove <= 0 || colToRemove <= 0) 
            throw new IllegalArgumentException("Ops, rowToRemove or colToRemove are in minus!!");
        

        // Creat array var
        int[][] result = new int[matrix.length][matrix[0].length];
        int fillingRow = 0; // var to new matrix

        for (int i = 0; i < matrix.length; i++) {
            if (i == rowToRemove - 1) {
                continue; // Terminate the current iteration and go to the next i
            }
            int fillingCol = 0; // start the colom for each rowToRemove by 0 , use fillingCol var for new matrix
            for (int j = 0; j < matrix[i].length; j++) {
                if (j == colToRemove - 1) {
                    continue; // Terminate the current iteration and go to the next j
                }
                result[fillingRow][fillingCol++] = matrix[i][j];

            }
            fillingRow++; // Increment the fillingRow for the new matrix
        }

        return result; // Return the result
    }

    // Square Matrix operations: Those operations are made to a square matrix where number of columns is equal to the 
    // number of rows. copyType represent the user choice if it is
    // D for Change Matrix to a diagonal matrix 
    // L for Change matrix to a lower triangular matrix 
    // U for Change matrix to a upper triangular matrix
    public int[][] copySquareMatrix(int[][] matrix, char copyType) throws IllegalArgumentException {

        // Check if the it is a square Matrix:
        if (matrix.length != matrix[0].length) 
            throw new IllegalArgumentException("Ops, It's not a square Matrix !!");
        

        copyType = Character.toUpperCase(copyType);
        // Determine the user input
       if(copyType == 'D' || copyType == 'L' || copyType == 'U')
                return doDiagonal(matrix, copyType);
       
        throw new IllegalArgumentException("Invalid input,\n Enter D for Change Matrix to a diagonal matrix \n L for Change matrix to a lower triangular matrix \n U for Change matrix to a upper triangular matrix!!");

    }

    // Function to get the diagonal matrix
    // TODO those diagonal methods look the same, try to have one method to handle three cases
    private int[][] doDiagonal(int[][] matrix, char copyType) {
        // Create array var
        int[][] result = new int[matrix.length][matrix[0].length];

        // Change Matrix to a diagonal matrix: all elements values are set to zero except the elements in the diagonal
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (copyType ==  'D' && i == j) // Change Matrix to a diagonal matrix: all elements values are set to zero except the elements in the diagonal
                    result[i][j] = matrix[i][j];
                else if (copyType ==  'L' && i >= j) // Change matrix to a lower triangular matrix: all values on top of the diagonal are set to 0
                    result[i][j] = matrix[i][j];
                else if (copyType ==  'U' && i <= j) // Change matrix to a upper triangular matrix: all values on bottom of the diagonal are set to 0.
                    result[i][j] = matrix[i][j];
                else 
                    result[i][j] = 0;      
            }
        }
        return result;
    }

    // Find a determinant of a (N x N) matrix, the determinant of (N x N) matrix is determined by 
    // the subtraction of diagonal multiplication
    public int determinant(int[][] matrix) throws IllegalArgumentException {
        // Check if the it is a square Matrix:
        if (matrix.length != matrix[0].length) {
            throw new IllegalArgumentException("Ops, It's not a square Matrix !!");
        }

        return determinant(matrix, matrix.length); // get a determinant of a (N x N) matrix

    }

    // Find determinant of matrix recursively.  
    private int determinant(int matrix[][], int dimension) {

        // If the matrix length is 1, then terminate the recursive function 
        if (dimension == 1) {
            return matrix[0][0];
        }

        int determinant = 0; // to store the result

        int signFlag = 1;   // to store the sign
        for (int i = 0; i < dimension; i++) {   // iterate for each element of first row
            int[][] sub = subMatrix(matrix, 1, i + 1); // Get Cofactor of m[0][i] by calling Submatrix function
            determinant += signFlag * matrix[0][i] * determinant(sub, dimension - 1);
            signFlag = -signFlag;  // Change the Sign
        }

        return determinant;
    }

    public void toString(int[][] matrix) {

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println();
        }
    }

}
